<?php require_once 'global.php' ?>
<?php require_once 'cabecalho.php' ?>
<?php
    try{
        $id = $_GET['id'];
        $categoria = new Categoria($id);
        $categoria->carregar_produtos();
        $listaProdutos = $categoria->carregar_produtos();
        $listaProdutos = $categoria->produtos;

    }catch (Exception $e){
        Erro::trata_erro($e);
    }
?>


<div class="row">
    <div class="col-md-12">
        <h2>Detalhe da Categoria</h2>
    </div>
</div>

<dl>
    <dt>ID</dt>
    <dd><?php echo $categoria->id?></dd>
    <dt>Nome</dt>
    <dd><?php echo $categoria->nome?></dd>
    <?php if(count($listaProdutos) > 0): ?>
    <dt>Produtos</dt>
    <dd>
        <ul>
            <?php foreach($listaProdutos as $linha): ?>
            <li><a href="/pdo/produtos-editar.php?id=<?=$linha['id']?>"><?=utf8_encode($linha['nome'])?></a></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </dd>
</dl>
<?php require_once 'rodape.php' ?>
