<?php

require_once 'global.php';
try {
    $id = $_GET['id'];
    $categoria = new Categoria($id);
    $categoria->excluir();
    header('location: categorias.php');
} catch (Exception $e) {
    Erro::trata_erro($e);
}
