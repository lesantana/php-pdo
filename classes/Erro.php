<?php

class Erro
{
    public static function trata_erro(Exception $e)
    {
        if(DEBUG){
            echo '<pre>';
            print_r($e);
        }else{
            echo $e->getMessage();
        }
        exit;
    }
}