<?php

class Produto
{
    public $id;
    public $nome;
    public $preco;
    public $quantidade;
    public $categoria_id;

    public function __construct($id = false)
    {
        if ($id) {
            $this->id = $id;
            $this->carregar();
        }
    }

    public static function listar()
    {
        $query = "SELECT p.id, p.nome, p.preco, p.quantidade, p.categoria_id, c.nome categoria_nome
                  FROM produtos p 
                  INNER JOIN categorias c ON c.id = p.categoria_id 
                  ORDER BY p.nome";
        $conexao = Conexao::pegarConexao();
        $resultado = $conexao->query($query);
        $lista = $resultado->fetchAll();
        return $lista;
    }

    public static function listarPorCategoria($categoria_id){


        $query = "SELECT id, nome, preco, quantidade FROM produtos WHERE categoria_id = :categria_id";
        $conexao = Conexao::pegarConexao();
        $stmt = $conexao->prepare($query);
        $stmt->bindValue(':categria_id', $categoria_id);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    private function carregar()
    {
        $query = "SELECT id, nome, preco, quantidade, categoria_id FROM produtos WHERE id = :id";
        $conexao = Conexao::pegarConexao();
        $stmt = $conexao->prepare($query);
        $stmt->bindValue(':id', $this->id);
        $stmt->execute();
        $linha = $stmt->fetch();
        $this->nome = $linha['nome'];
        $this->preco = $linha['preco'];
        $this->quantidade = $linha['quantidade'];
        $this->categoria_id = $linha['categoria_id'];
    }

    public function inserir()
    {
        $query = "INSERT 
                  INTO produtos (nome,preco,quantidade,categoria_id)
                  VALUES(:nome,:preco,:quantidade,:categoria_id)";
        $conexao = Conexao::pegarConexao();
        $stmt = $conexao->prepare($query);
        $stmt->bindValue(':nome',$this->nome);
        $stmt->bindValue(':preco',$this->preco);
        $stmt->bindValue(':quantidade',$this->quantidade);
        $stmt->bindValue(':categoria_id',$this->categoria_id);
        $stmt->execute();
    }

    public function atualizar()
    {

        $query = "UPDATE produtos SET 
                  nome = :nome, 
                  quantidade = :quantidade, 
                  preco = :preco, 
                  categoria_id = :categoria_id 
                  WHERE id =:id";

        $conexao = Conexao::pegarConexao();
        $stmt = $conexao->prepare($query);
        $stmt->bindValue(':nome', $this->nome);
        $stmt->bindValue(':quantidade', $this->quantidade);
        $stmt->bindValue(':preco', $this->preco);
        $stmt->bindValue(':categoria_id', $this->categoria_id);
        $stmt->bindValue(':id', $this->id);
        $stmt->execute();
    }

    public function excluir()
    {
        $query = "DELETE FROM produtos WHERE id=:id";
        $conexao = Conexao::pegarConexao();
        $stmt = $conexao->prepare($query);
        $stmt->bindValue(':id',$this->id);
        $stmt->execute();
    }
}