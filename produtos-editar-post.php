<?php

require_once 'global.php';

try{
    $id = $_POST['id'];
    $nome = $_POST['nome'];
    $quantidade = $_POST['quantidade'];
    $preco = $_POST['preco'];
    $categoria_id = $_POST['categoria_id'];



    $produto = new Produto();
    $produto->id = $id;
    $produto->nome = $nome;
    $produto->quantidade = $quantidade;
    $produto->preco = $preco;
    $produto->categoria_id = $categoria_id;

    $produto->atualizar();

    header('location:produtos.php');

}catch (Exception $e){
    Erro::trata_erro($e);
}