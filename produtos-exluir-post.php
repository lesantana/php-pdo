<?php
require_once 'global.php';

try {
    $id = $_GET['id'];
    $produto = new Produto($id);
    $produto->excluir();

    header('location:produtos.php');
} catch (Exception $e) {
    Erro::trata_erro($e);
}
