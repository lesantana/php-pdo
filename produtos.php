<?php require_once 'global.php' ?>
<?php require_once 'cabecalho.php' ?>
<?php
try {
    $lista = Produto::listar();
} catch (Exception $e) {
    Erro::trata_erro($e);
}

?>

<div class="row">
    <div class="col-md-12">
        <h2>Produtos</h2>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <a href="produtos-criar.php" class="btn btn-info btn-block">Crair Novo Produto</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php if (count($lista) > 0): ?>
            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Preço</th>
                    <th>Quantidade</th>
                    <th>Categoria</th>
                    <th class="acao">Editar</th>
                    <th class="acao">Excluir</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($lista as $lista): ?>
                    <tr>
                        <td><?= $lista['id'] ?></td>
                        <td><?= utf8_encode($lista['nome']) ?></td>
                        <td>R$ <?= $lista['preco'] ?></td>
                        <td><?= $lista['quantidade'] ?></td>
                        <td><?= $lista['categoria_nome'] ?></td>
                        <td><a href="/pdo/produtos-editar.php?id=<?= $lista['id'] ?>" class="btn btn-info">Editar</a></td>
                        <td><a href="/pdo/produtos-exluir-post.php?id=<?= $lista['id'] ?>" class="btn btn-danger">Excluir</a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p>Nenhum produto cadastrado</p>
        <?php endif; ?>
    </div>
</div>
<?php require_once 'rodape.php' ?>
